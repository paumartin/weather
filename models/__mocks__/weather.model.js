const weatherMockedData = require('./mocked-data/weather');

const createWeather = jest.fn((weather, callback) => {
  callback(null, weather);
});

const findWeatherByAddressAndLastHour = jest.fn((addressId, callback) => {
  callback(null, weatherMockedData.findWeatherByAddressAndLastHour.weatherFoundWithPrecipitation);
});

module.exports.createWeather = createWeather;
module.exports.findWeatherByAddressAndLastHour = findWeatherByAddressAndLastHour;
