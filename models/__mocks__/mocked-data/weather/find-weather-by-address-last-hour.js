const weatherFoundWithPrecipitation = {
  _id: 'mocked_weather_id',
  addressId: 'address_id',
  location: {
    lat: 0,
    lng: 0
  },
  weatherDescription: 'mocked_weather_description',
  precipitation: true
};
const weatherFoundWithNoPrecipitation = {
  _id: 'mocked_weather_id',
  addressId: 'address_id',
  location: {
    lat: 0,
    lng: 0
  },
  weatherDescription: 'mocked_weather_description',
  precipitation: true
};
const weatherNotFound = null;

module.exports.weatherFoundWithPrecipitation = weatherFoundWithPrecipitation;
module.exports.weatherFoundWithNoPrecipitation = weatherFoundWithNoPrecipitation;
module.exports.weatherNotFound = weatherNotFound;
