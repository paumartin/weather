const findAddressFound = {
  _id: 'mocked_address_id',
  street: 'street name',
  streetNumber: 'street number',
  town: 'town name',
  postalCode: 'postal code',
  country: 'country name',
  location: {
    lat: 0,
    lng: 0
  },
  createdAt: new Date(),
  updatedAt: new Date()
};

const findAddressNotFound = null;

module.exports.findAddressFound = findAddressFound;
module.exports.findAddressNotFound = findAddressNotFound;
