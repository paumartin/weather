const coordinatesFromValidAddress = {
  results: [
    {
      geometry: {
        location: {
          lat: 0,
          lng: 0
        }
      }
    }
  ]
};

const coordinatesFromInvalidAddress = {
  results: []
};

module.exports.coordinatesFromValidAddress = coordinatesFromValidAddress;
module.exports.coordinatesFromInvalidAddress = coordinatesFromInvalidAddress;
