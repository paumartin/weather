const weatherWithNoPrecipitations = {
  'coord': {
  'lon': 1.13,
    'lat': 41.14
},
  'weather': [
  {
    'id': 801,
    'main': 'Clouds',
    'description': 'few clouds',
    'icon': '02d'
  }
],
  'base': 'stations',
  'main': {
  'temp': 282.06,
    'pressure': 1011,
    'humidity': 50,
    'temp_min': 279.26,
    'temp_max': 284.26
},
  'visibility': 10000,
  'wind': {
  'speed': 6.2,
    'deg': 270
},
  'clouds': {
  'all': 20
},
  'dt': 1573920785,
  'sys': {
  'type': 1,
    'id': 6427,
    'country': 'ES',
    'sunrise': 1573886645,
    'sunset': 1573922191
},
  'timezone': 3600,
  'id': 3111933,
  'name': 'Reus',
  'cod': 200
};

const weatherWithPrecipitations = {
  'coord': {
    'lon': -2.2,
    'lat': 43.05
  },
  'weather': [
    {
      'id': 501,
      'main': 'Rain',
      'description': 'moderate rain',
      'icon': '10d'
    }
  ],
  'base': 'stations',
  'main': {
    'temp': 280.69,
    'pressure': 1012,
    'humidity': 75,
    'temp_min': 277.59,
    'temp_max': 282.59
  },
  'visibility': 10000,
  'wind': {
    'speed': 4.6,
    'deg': 270
  },
  'clouds': {
    'all': 75
  },
  'dt': 1573921024,
  'sys': {
    'type': 1,
    'id': 6438,
    'country': 'ES',
    'sunrise': 1573887729,
    'sunset': 1573922706
  },
  'timezone': 3600,
  'id': 3128436,
  'name': 'Beasain',
  'cod': 200
};

module.exports.weatherWithNoPrecipitations = weatherWithNoPrecipitations;
module.exports.weatherWithPrecipitations = weatherWithPrecipitations;
