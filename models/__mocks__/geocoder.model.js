const geocoderMockedData = require('./mocked-data/geocoder');

const geocodeAddress = jest.fn((address, callback) => {
  const coordinates = geocoderMockedData.geocodeAddress.coordinatesFromValidAddress;
  return callback(null, { ...coordinates });
});

module.exports.geocodeAddress = geocodeAddress;
