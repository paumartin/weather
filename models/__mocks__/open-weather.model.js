const openWeatherMockedData = require('./mocked-data/open-weather');

const getWeatherByCoordinates = jest.fn((coordinates, callback) => {
  return callback(null, clone(openWeatherMockedData.getWeatherByCoordinates.weatherWithNoPrecipitations));
});

module.exports.getWeatherByCoordinates = getWeatherByCoordinates;

function clone(object) {
  return JSON.parse(JSON.stringify(object));
}
