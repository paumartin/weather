const addressMockedData = require('./mocked-data/address');

const createAddress = jest.fn((address, callback) => {
  return callback(null, address);
});

const findAddress = jest.fn((address, callback) => {
  return callback(null, addressMockedData.findAddress.findAddressFound);
});

module.exports.createAddress = createAddress;
module.exports.findAddress = findAddress;
