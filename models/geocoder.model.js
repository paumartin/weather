const { get } = require('../utils/request.utils');

const { GOOGLE_API_KEY } = require('../configs/config');

module.exports.geocodeAddress = geocodeAddress;

function geocodeAddress(address, callback) {
  console.log('Fetching address from google geocoder');
  const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${GOOGLE_API_KEY}`;
  get(url, callback);
}
