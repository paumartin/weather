const { get } = require('../utils/request.utils');

const { OPEN_WEATHER_API_KEY } = require('../configs/config');

module.exports.getWeatherByCoordinates = getWeatherByCoordinates;

function getWeatherByCoordinates(coordinates, callback) {
  console.log('Fetching weather from open weather API');
  const { lat, lng } = coordinates;
  const url = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&APPID=${OPEN_WEATHER_API_KEY}`;
  get(url, callback);
}
