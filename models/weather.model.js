const mongoose = require('mongoose');

const weatherSchema = require('./schemas/weather.schema');

const Weather = mongoose.model('weather', weatherSchema);

module.exports.createWeather = createWeather;
module.exports.findWeatherByAddressAndLastHour = findWeatherByAddressAndLastHour;

function createWeather(weatherData, callback) {
  const weather = new Weather(weatherData);
  weather.save(callback);
}

function findWeatherByAddressAndLastHour(addressId, callback) {
  const date = new Date();
  date.setHours(date.getHours() - 1);

  const query = {
    addressId,
    createdAt: { $gte: date }
  };
  Weather.findOne(query)
    .sort({ 'createdAt': -1 })
    .exec(callback);
}
