const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const locationSchema = require('./location.schema');

const addressSchema = new Schema({
  street: {
    type: String,
    required: true
  },
  streetNumber: {
    type: String,
    required: true
  },
  town: {
    type: String,
    required: true
  },
  postalCode: {
    type: String,
    required: true
  },
  country: {
    type: String,
    required: true
  },
  location: {
    type: locationSchema
  }
}, {
  timestamps: true
});

addressSchema.index({
  street: 1,
  streetNumber: 1,
  town: 1,
  postalCode: 1,
  country: 1
}, { unique: true });

module.exports = addressSchema;
