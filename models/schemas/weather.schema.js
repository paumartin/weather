const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const locationSchema = require('./location.schema');

const weatherSchema = new Schema({
  addressId: {
    type: Schema.Types.ObjectId
  },
  location: {
    type: locationSchema,
    required: true
  },
  weatherDescription: {
    type: String
  },
  precipitation: {
    type: Boolean
  }
}, {
  timestamps: true
});

weatherSchema.index({ createdAt: -1 });

module.exports = weatherSchema;
