const mongoose = require('mongoose');

const addressSchema = require('./schemas/address.schema');

const Address = mongoose.model('address', addressSchema);

module.exports.createAddress = createAddress;
module.exports.findAddress = findAddress;
module.exports.getAllAddresses = getAllAddresses;

function createAddress(addressData, callback) {
  const address = new Address(addressData);
  address.save(callback);
}

function findAddress(address, callback) {
  const query = { ...address };
  Address.findOne(query, callback);
}

function getAllAddresses(callback) {
  Address.find({}, callback);
}
