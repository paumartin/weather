const cookieParser = require('cookie-parser');
const express = require('express');
const mongoose = require('mongoose');
const logger = require('morgan');

const { MONGODB_PATH } = require('./configs/config');

const addressRoutes = require('./routes/address.routes');

// Connect to mongodb
connectMongoDb();

// Init express, middlewares and routes
const app = initExpress();

module.exports = app;

function initExpress() {
  const app = express();

  app.use(logger('dev'));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(cookieParser());

  app.use('/address', addressRoutes);
  return app;
}

function connectMongoDb() {
  const mongodbUrl = `mongodb://localhost:27017/${MONGODB_PATH}`;
  const mongodbOptions = {
    autoReconnect: true,
    useCreateIndex: true,
    useNewUrlParser: true,
    reconnectInterval: 2000,
    reconnectTries: 10,
    useUnifiedTopology: true
  };
  mongoose.connect(mongodbUrl, mongodbOptions);

  mongoose.connection.on('connecting', () => {
    console.log('Connecting to MongoDB...');
  });
  mongoose.connection.on('error', (error) => {
    console.log('Error in MongoDb connection: ' + error);
  });
  mongoose.connection.on('connected', () => {
    console.log('MongoDB connected!');
  });
}
