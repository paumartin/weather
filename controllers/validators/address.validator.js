const httpStatus = require('http-status');

module.exports.validateAddress = validateAddress;

function validateAddress(address) {
  const requiredFields = [
    'street',
    'streetNumber',
    'town',
    'postalCode',
    'country'
  ];
  const missingField = requiredFields.find(fieldName => !address[fieldName]);
  if (missingField) {
    return {
      errorCode: httpStatus.BAD_REQUEST,
      errorBody: { message: `Missing field: ${missingField}`}
    };
  }
}
