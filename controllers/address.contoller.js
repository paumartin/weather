const httpStatus = require('http-status');

const addressValidator = require('./validators/address.validator');

const addressService = require('../services/address.service');

module.exports.validateAddress = validateAddress;
module.exports.checkAddressWeather = checkAddressWeather;
module.exports.batchProcess = batchProcess;

function validateAddress(req, res) {
  const invalidAddress = addressValidator.validateAddress(req.body);
  if (invalidAddress) {
    return res.status(invalidAddress.errorCode).json(invalidAddress.errorBody);
  }
  addressService.validateAddress(req.body, (err, foundAddress) => {
    if (err) {
      const errorCode = Object.keys(httpStatus).includes(err.toString()) ? err : httpStatus.INTERNAL_SERVER_ERROR;
      return res.status(errorCode).send();
    }
    res.status(httpStatus.OK).json({ validAddress: !!foundAddress });
  });
}

function checkAddressWeather(req, res) {
  const invalidAddress = addressValidator.validateAddress(req.body);
  if (invalidAddress) {
    return res.status(invalidAddress.errorCode).json(invalidAddress.errorBody);
  }
  const options = {};
  if (req.query.disableCache) {
    options.disableCache = true;
  }
  addressService.checkWeather(req.body, options, (err, weather) => {
    if (err) {
      const errorCode = Object.keys(httpStatus).includes(err.toString()) ? err : httpStatus.INTERNAL_SERVER_ERROR;
      return res.status(errorCode).send();
    }
    res.status(httpStatus.OK).json({ weather });
  });
}

function batchProcess(req, res) {
  const options = {};
  if (req.query.disableCache) {
    options.disableCache = true;
  }
  addressService.batchProcess(options, () => {});
  res.status(httpStatus.OK).send();
}
