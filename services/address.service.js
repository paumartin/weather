const async = require('async');

const Address = require('../models/address.model');

const geocoderService = require('./geocoder.service.js');
const notificationService = require('./notification.service');
const weatherService = require('./weather.service');

module.exports.validateAddress = validateAddress;
module.exports.checkWeather = checkWeather;
module.exports.batchProcess = batchProcess;

function createAddress(address, callback) {
  Address.createAddress(address, callback);
}

function validateAddress(address, callback) {
  Address.findAddress(address, handleFindAddress);

  // Inner functions
  function handleFindAddress(err, foundAddress) {
    if (err) {
      return callback(err);
    }
    if (foundAddress) {
      return callback(null, foundAddress);
    }
    geocoderService.validateAddress(address, handleValidateAddress);
  }
  function handleValidateAddress(err, coordinates) {
    if (err) {
      return callback(err);
    }
    if (!coordinates) {
      return callback(null, null);
    }
    address.location = coordinates;
    createAddress(address, callback);
  }
}

function checkWeather(address, options, callback) {
  async.waterfall([
    checkWeatherValidateAddress,
    checkWeatherByCoordinates
  ], handleWaterfall);

  // Inner functions
  function checkWeatherValidateAddress(cb) {
    validateAddress(address, cb);
  }
  function checkWeatherByCoordinates(validatedAddress, cb) {
    const coordinates = validatedAddress.location;
    const params = { addressId: validatedAddress._id, coordinates };
    weatherService.findWeather(params, options, cb);
  }
  function handleWaterfall(err, weather) {
    if (err) {
      return callback(err)
    }
    callback(null, weather.weatherDescription);
  }
}

function batchProcess(options, callback) {
  async.waterfall([
    getAllAddresses,
    checkWeatherForAddresses
  ], callback);

  // Inner functions
  function getAllAddresses(cb) {
    Address.getAllAddresses(cb)
  }
  function checkWeatherForAddresses(addresses, cb) {
    async.eachSeries(
      addresses,
      checkWeatherForAddress,
      cb
    );
  }

  function checkWeatherForAddress(address, cb) {
    const coordinates = address.location;
    const params = { addressId: address._id, coordinates };
    weatherService.findWeather(params, options, (err, result) => {
      if (err) {
        return cb(err);
      }
      if (result.precipitation) {
        notificationService.sendEmailNotification(address, result);
      }
      cb();
    });
  }
}
