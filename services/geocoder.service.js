const httpStatus = require('http-status');

const Geocoder = require('../models/geocoder.model');

module.exports.validateAddress = validateAddress;

function validateAddress(address, callback) {
  const addressString = Object.values(address).join(',');
  Geocoder.geocodeAddress(addressString, handleGeocodeAddress);

  // Inner functions
  function handleGeocodeAddress(err, result) {
    if (err) {
      return callback(err);
    }
    const coordinates = getCoordinatesFromGeocoderResult(result);
    if (!coordinates) {
      return callback('ADDRESS_INVALID');
    }
    callback(err, coordinates);
  }
}

function getCoordinatesFromGeocoderResult(result) {
  return result.results &&
    result.results.length &&
    result.results[0].geometry &&
    result.results[0].geometry.location;
}
