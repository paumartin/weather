const OpenWeather = require('../models/open-weather.model');

const openWeatherService = require('./open-weather.service');

const openWeatherMockedData = require('../models/__mocks__/mocked-data/open-weather');

jest.mock('../models/open-weather.model');

describe('Open weather API', () => {

  describe('given weather information', () => {

    const coordinates = {};

    describe('when it is not raining', () => {
      it('precipitation should be false', (done) => {
        openWeatherService.getWeatherByCoordinates(coordinates, (err, weather) => {
          if (err) {
            return done(err);
          }
          expect(weather.precipitation).toBe(false);
          done(null);
        });
      })
    });

    describe('when it is not raining', () => {
      it('precipitation should be true', (done) => {
        mockGetWeatherByCoordinatesHasPrecipitation();
        openWeatherService.getWeatherByCoordinates(coordinates, (err, weather) => {
          if (err) {
            return done(err);
          }
          expect(weather.precipitation).toBe(true);
          done(null);
        });
      })
    });

  });

});

function mockGetWeatherByCoordinatesHasPrecipitation() {
  OpenWeather.getWeatherByCoordinates.mockImplementationOnce((coordinates, callback) => {
    return callback(null, openWeatherMockedData.getWeatherByCoordinates.weatherWithPrecipitations);
  });
}
