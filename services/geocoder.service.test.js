const Geocoder = require('../models/geocoder.model');

const geocoderService = require('./geocoder.service.js');

const geocoderMockedData = require('../models/__mocks__/mocked-data/geocoder');

jest.mock('../models/geocoder.model');

describe('Geocoder google API', () => {

  describe('given address of a location', () => {

    const address = {};

    describe('when the address is valid', () => {
      it('coordinates should be returned', (done) => {
        geocoderService.validateAddress(address, (err, coordinates) => {
          if (err) {
            return done(err);
          }
          expect(coordinates).toHaveProperty('lat');
          expect(coordinates).toHaveProperty('lng');
          done(null);
        });
      });
    });

    describe('when the address does not exists', () => {
      it('ADDRESS_INVALID error should be returned', (done) => {
        mockGeocodeAddressInvalidAddress();
        geocoderService.validateAddress(address, (err) => {
          expect(err).toBe('ADDRESS_INVALID');
          done(null);
        });
      });
    });

  });

});

function mockGeocodeAddressInvalidAddress() {
  Geocoder.geocodeAddress.mockImplementationOnce((address, callback) => {
    return callback(null, geocoderMockedData.geocodeAddress.coordinatesFromInvalidAddress);
  });
}
