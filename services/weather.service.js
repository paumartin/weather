const async = require('async');

const Weather = require('../models/weather.model');

const openWeatherService = require('./open-weather.service');

module.exports.createWeather = createWeather;
module.exports.findWeather = findWeather;

function createWeather(weather, callback) {
  Weather.createWeather(weather, callback);
}

function findWeather(params, options, callback) {
  const { addressId } = params;
  const { disableCache } = options;

  if (!addressId || disableCache) {
    return getOpenWeatherService(params, callback);
  }

  Weather.findWeatherByAddressAndLastHour(addressId, (err, foundWeather) => {
    if (err) {
      return callback(err);
    }
    if (foundWeather) {
      return callback(null, foundWeather);
    }
    getOpenWeatherService(params, callback);
  });
}

function getOpenWeatherService(params, callback) {
  const { addressId, coordinates } = params;
  async.waterfall([
    getWeatherFromOpenWeather,
    saveWeather
  ], callback);

  // Inner functions
  function getWeatherFromOpenWeather(cb) {
    openWeatherService.getWeatherByCoordinates(coordinates, cb);
  }
  function saveWeather(weather, cb) {
    weather.addressId = addressId;
    weather.location = coordinates;
    createWeather(weather, cb);
  }
}
