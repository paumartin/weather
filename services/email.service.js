const emailService = require('nodemailer');

const { NODEMAILER_SENDER_EMAIL, NODEMAILER_SENDER_PASSWORD, NOTIFICATIONS_EMAIL_ADDRESS } = require('../configs/config');

class NodemailerInstance {

  constructor() {
    this.transport = emailService.createTransport({
      service: 'gmail',
      auth: {
        user: NODEMAILER_SENDER_EMAIL,
        pass: NODEMAILER_SENDER_PASSWORD
      }
    })
  }

  sendEmail(text) {
    const mail = {
      from: NODEMAILER_SENDER_EMAIL,
      to: NOTIFICATIONS_EMAIL_ADDRESS,
      subject: 'Precipitation detected',
      text
    };
    this.transport.sendMail(mail, (err) => {
      if (err) {
        console.log('There was an error sending the email');
        return console.log(err);
      }
      console.log('Email sent successfully');
    });
  }
}

module.exports = new NodemailerInstance();
