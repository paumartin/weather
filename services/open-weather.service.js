const OpenWeather = require('../models/open-weather.model');

module.exports.getWeatherByCoordinates = getWeatherByCoordinates;

function getWeatherByCoordinates(coordinates, callback) {
  OpenWeather.getWeatherByCoordinates(coordinates, handleGetWeather);

  // Inner functions
  function handleGetWeather(err, result) {
    if (err) {
      return callback(err);
    }
    const weatherResult = parseWeatherResult(result);
    callback(null, weatherResult);
  }
  function parseWeatherResult(result) {
    const weatherMains = result.weather &&
      result.weather.length &&
      result.weather.map(weather => weather.main.toLowerCase());

    const weatherDescription = result.weather &&
      result.weather.length &&
      result.weather.map(weather => weather.description).join(', ');

    const precipitation = checkPrecipitation(weatherMains);
    return { weatherDescription, precipitation };
  }
}

function checkPrecipitation(weatherMains) {
  const precipitationsToFind = [
    'rain',
    'snow',
    'thunderstorm'
  ];
  const weatherMainsSet = new Set(weatherMains);
  return precipitationsToFind.some(precipitationToFind => weatherMainsSet.has(precipitationToFind));
}
