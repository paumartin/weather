const Address = require('../models/address.model.js');
const Geocoder = require('../models/geocoder.model');
const OpenWeather = require('../models/open-weather.model');
const Weather = require('../models/weather.model');

const addressService = require('./address.service');

const addressMockedData = require('../models/__mocks__/mocked-data/address');
const geocoderMockedData = require('../models/__mocks__/mocked-data/geocoder');
const weatherMockedData = require('../models/__mocks__/mocked-data/weather');

// TODO: Mock geocoder and weather service instead of models
jest.mock('../models/address.model');
jest.mock('../models/geocoder.model');
jest.mock('../models/open-weather.model');
jest.mock('../models/weather.model');

describe('Address service', () => {

  // Validate address
  describe('validate address method', () => {
    describe('given an address object', () => {
      const address = { street: 'given_address_street' };

      // Address is stored in the db
      describe('when the address is stored in the db', () => {
        it('geocoder should not be called', (done) => {
          addressService.validateAddress(address, (err) => {
            if (err) {
              return done(err);
            }
            expect(Geocoder.geocodeAddress.mock.calls.length).toBe(0);
            done(null);
          });
        });
        it('mocked address should be returned', (done) => {
          addressService.validateAddress(address, (err, address) => {
            if (err) {
              return done(err);
            }
            expect(address._id).toBe('mocked_address_id');
            done(null);
          });
        });
      });

      // Address is not stored in the db
      describe('when the address is not stored in the db', () => {
        describe('given address is valid', () => {
          it('should return given address with valid coordinates', (done) => {
            mockValidateAddressNotStored();
            addressService.validateAddress(address, (err, validatedAddress) => {
              if (err) {
                return done(err);
              }
              expect(validatedAddress.street).toBe('given_address_street');
              expect(validatedAddress.location).toHaveProperty('lat');
              expect(validatedAddress.location).toHaveProperty('lng');
              done(null);
            });
          });
        });
        describe('given address is invalid', () => {
          it('should return error ADDRESS_INVALID', (done) => {
            mockValidateAddressNotStored();
            mockGeocodeAddressInvalidAddress();
            addressService.validateAddress(address, (err) => {
              expect(err).toBe('ADDRESS_INVALID');
              done(null);
            })
          });
        });
      });
    });
  });

  // Check weather
  describe('check weather method', () => {
    describe('given address is valid and stored in the db', () => {
      const address = {};
      const options = {};
      const noCacheOptions = { disableCache: true };

      // Weather is stored in the db
      describe('weather information is stored in the db', () => {
        it('should call weather model', (done) => {
          addressService.checkWeather(address, options, (err) => {
            if (err) {
              return done(err);
            }
            expect(Weather.findWeatherByAddressAndLastHour.mock.calls.length).toBe(1);
            expect(OpenWeather.getWeatherByCoordinates.mock.calls.length).toBe(0);
            done(null);
          });
        });
      });

      // Weather is not stored in the db
      describe('weather information is not stored in the db', () => {
        it('should all both open weather and weather model', (done) => {
          mockFindWeatherNotFound();
          addressService.checkWeather(address, options, (err) => {
            if (err) {
              return done(err);
            }
            expect(Weather.findWeatherByAddressAndLastHour.mock.calls.length).toBe(1);
            expect(OpenWeather.getWeatherByCoordinates.mock.calls.length).toBe(1);
            done(null);
          });
        });
      });

      // Disable cache param is provided
      describe('disable cache param is provided', () => {
        it('should call open weather model', (done) => {
          addressService.checkWeather(address, noCacheOptions, (err) => {
            if (err) {
              return done(err);
            }
            expect(Weather.findWeatherByAddressAndLastHour.mock.calls.length).toBe(0);
            expect(OpenWeather.getWeatherByCoordinates.mock.calls.length).toBe(1);
            done(null);
          });
        });
      });
    });
  });
});

function mockValidateAddressNotStored() {
  Address.findAddress.mockImplementationOnce((address, callback) => {
    return callback(null, addressMockedData.findAddress.findAddressNotFound);
  })
}

function mockGeocodeAddressInvalidAddress() {
  Geocoder.geocodeAddress.mockImplementationOnce((address, callback) => {
    return callback(null, geocoderMockedData.geocodeAddress.coordinatesFromInvalidAddress);
  });
}

function mockFindWeatherNotFound() {
  Weather.findWeatherByAddressAndLastHour.mockImplementationOnce((addressId, callback) => {
    return callback(null, weatherMockedData.findWeatherByAddressAndLastHour.weatherNotFound);
  });
}
