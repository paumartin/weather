const emailService = require('./email.service');

module.exports.sendEmailNotification = sendEmailNotification;

function sendEmailNotification(address, weather) {
  const { street, streetNumber, town, postalCode, country } = address;
  const filteredAddress = { street, streetNumber, town, postalCode, country };
  const addressString = Object.values(filteredAddress).join(', ');

  emailService.sendEmail(addressString);
}
