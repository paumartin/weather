const httpStatus = require('http-status');

const request = require('request');

module.exports.get = get;
module.exports.post = post;

function get(url, callback) {
  request({
    method: 'GET',
    url,
    json: true
  }, handleRequest(callback));
}

function post(url, body, callback) {
  request({
    method: 'POST',
    url,
    body,
    json: true
  }, handleRequest(callback));
}

function handleRequest(callback) {
  return (err, res, body) => {
    if (err) {
      return callback(httpStatus.INTERNAL_SERVER_ERROR);
    }
    if (res.statusCode >= 400) {
      return callback(res.statusCode);
    }
    callback(null, body);
  }
}
