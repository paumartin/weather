const express = require('express');
const router = express.Router();

const addressController = require('../controllers/address.contoller');

router.route('/validate')
  .post(addressController.validateAddress);

router.route('/check-weather')
  .post(addressController.checkAddressWeather);

router.route('/batch-process')
  .post(addressController.batchProcess);

module.exports = router;
